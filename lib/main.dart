import 'package:flutter/material.dart';

void main() {
  runApp(myApp());
}

class myApp extends StatefulWidget {
  @override
  _myAppState createState() => _myAppState();
}

class _myAppState extends State<myApp> {
  int number = 0;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Basic Statefull"),
        ),
        body: Column(
          children: [
            Row(
              children: [
                Text(number.toString()),
              ],
            ),
            Row(
              children: [
                TextButton(
                    onPressed: () {
                      setState(() {
                        number--;
                      });
                    },
                    child: Text("Decrement")),
                TextButton(
                    onPressed: () {
                      setState(() {
                        number++;
                      });
                    },
                    child: Text("Increment"))
              ],
            )
          ],
        ),
      ),
    );
  }
}
